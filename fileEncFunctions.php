<?php

/* 
 * All right reserved to: Jan Cejka <posta@jancejka.cz>, http://jancejka.cz
 */

function dirCrawler($dir, $destination, &$hashes) {
    global $passphrase; // běžně v OOP nepoužívám, ale pro příklad stačí :-)
    
    $handle = opendir($dir);
    while (($file = readdir($handle)) !== false) {
        if ($file == '.' || $file == '..') {
            continue;
        }
        $filepath = $dir == '.' ? $file : $dir . '/' . $file;
        $filepathTo = ($destination == '.' ? $file : $destination) . '/' . $file;
        if (is_link($filepath))
            continue;
        if (is_file($filepath)) {
            if( !file_exists($destination) ) {
                mkdir($destination);
            }
            $fileHash = sha1(file_get_contents($filepath) . $passphrase);
            if( !isset($hashes[$filepath]) || ($hashes[$filepath] != $fileHash) ) {
                $hashes[$filepath] = $fileHash;
                echo("encrypt file '$filepath' to '$filepathTo'\n");
                copyEncryptedFile($filepath, $filepathTo, $passphrase);
            }
        }
        else if (is_dir($filepath))
            dirCrawler($filepath, $filepathTo, $hashes);
    }
    closedir($handle);
} 

function copyEncryptedFile( $source, $destination, $passphrase ) {
    $iv = substr(md5('iv'.$passphrase, true), 0, 8);
    $key = substr(md5('pass1'.$passphrase, true) . 
                   md5('pass2'.$passphrase, true), 0, 24);
    $opts = array('iv'=>$iv, 'key'=>$key);

    $fp = fopen($destination, 'wb');
    stream_filter_append($fp, 'mcrypt.tripledes', STREAM_FILTER_WRITE, $opts);
    fwrite($fp, file_get_contents($source));
    fclose($fp);
}

function readHashes( $filepath ) {
    if( !file_exists($filepath) )
        return array();

    $fileContent = file_get_contents($filepath);
    $hashes = unserialize( $fileContent );

    return $hashes;
}

function writeHashes( $filepath, $hashes ) {
    file_put_contents( $filepath, serialize($hashes) );
}
