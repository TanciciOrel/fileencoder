<?php

/* 
 * All right reserved to: Jan Cejka <posta@jancejka.cz>, http://jancejka.cz
 */

if( sizeof( $argv ) < 4 ) {
    echo("použití scriptu:\nphp fileEnc.php <zdrojový adresář> <cílový adresář> <heslo>");
    exit();
}

$passphrase = $argv[3];
$fileHashDB = './filehash.txt';

require_once './fileEncFunctions.php';

$hashes = readHashes( $fileHashDB );
dirCrawler( $argv[1], $argv[2], $hashes );
writeHashes( $fileHashDB, $hashes );
